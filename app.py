import librosa
import librosa.display
import numpy as np
import pandas as pd
import scipy.signal
import os
from joblib import dump, load
from IPython.display import Audio
import sounddevice as sd
from scipy.io.wavfile import write

def register():
    fs = 44100  # Sample rate
    seconds = 5  # Duration of recording
    
    print("A vous")
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    sd.wait()  # Wait until recording is finished
    print("merci")
    write('output.wav', fs, myrecording)
    return 'output.wav'

def silence_sup(y_voice,fl=2048,tdb=45):
    voice_intervals = librosa.effects.split(y_voice, top_db = tdb ,frame_length=fl)
    sound_intervals =[y_voice[voice_intervals[i][0]:voice_intervals[i][1]] for i in range(0,len(voice_intervals))]
    silenceless_voice = np.concatenate(sound_intervals)
    return silenceless_voice

def generate_dataset(silenceless_voice, sr, label, nm = 128):
    # generate Mel spectrogram
    spec = librosa.feature.melspectrogram(y=silenceless_voice, sr=sr, n_mels=nm)
    # generate DataFrame from spectrogram (columns: frequencies, rows: analysis frames)
    X = pd.DataFrame(spec.T)
    # generate Series for the labels (same label for all examples)
    y = pd.Series(label).repeat(len(X))
    return X, y

def analyse(register=register()):
    pipe= load('./models.joblib')
    audio, sr = librosa.load(register)
    silence = silence_sup(audio)
    X,_= generate_dataset(silence, sr,'?')
    pred=pipe.predict(X)
    labels, counts = np.unique(pred, return_counts=True)
    resultat=labels[np.argmax(counts)]
    
    return resultat

